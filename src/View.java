import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;

public class View extends JFrame {
	
	
	
	private Model v_model;
	//private JTextArea obszar_polecenia;
	//private JButton wykonaj;
	private JButton usunWierszButton;
	private JButton dodajWierszButton;
	private JButton Nowe;
	private JPanel presentation_pane;
	private JTable table;
	private JScrollPane scrollPane;
	private JList listaTabel;
	private noweMenu Menu;
	

	public View(Model model) {
		v_model = model;

		// budujemy okno;
		try {
            // Set cross-platform Java L&F (also called "Metal")
        UIManager.setLookAndFeel(
        		"com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
	    } 
	    catch (UnsupportedLookAndFeelException e) {
	       // handle exception
	    }
	    catch (ClassNotFoundException e) {
	       // handle exception
	    }
	    catch (InstantiationException e) {
	       // handle exception
	    }
	    catch (IllegalAccessException e) {
	       // handle exception
	    }
	        	   

		createGui();
		
	}



	protected void createGui() {
		// ustalenie tytu�u okna
		setTitle("Simple Postgres CRUD");
		// ustalenie rozk�adu - je�li trzeba, np:
		setLayout(new BorderLayout());

		// tworzenie komponent�w
		Container powZAwartosci = getContentPane();

		// Ustalenie domy�lnej operacji zamkni�cia okna
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// ustalenie rozmiar�w okna, np.:

		// ustalenie po�o�enia okna np. wycentrowanie
		setLocationRelativeTo(null);

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
		Point newLocation = new Point(middle.x - (this.getWidth() / 2 + 400),
				middle.y - (this.getHeight() / 2 + 300));
		this.setLocation(newLocation);
		// pokazanie okna
		setVisible(true);

		create_Menu();
		create_info_pane();

		create_both();
		

		pack();
	}

	/**
	 * Funkcje Tworz�ce poszczegolne elementy GUI
	 */
	void create_Menu() {
		// utw�rz menu Plik
		 Menu = new noweMenu(this);

	}

	void create_info_pane() {

		nowy_info_pane info_pane = new nowy_info_pane(this);

		// lm = info_pane.pobierz_model_listy();
		Nowe = info_pane.wez_przycisk();
		listaTabel = info_pane.wez_liste();
	}

	void create_both() {
		add_execute_pane_and_presentation_pane p = new add_execute_pane_and_presentation_pane(
				this);
		
		usunWierszButton = p.usunWierszButton();
		dodajWierszButton = p.dodajWierszButton();
		
		table = p.get_table();
		scrollPane = p.get_JScrollPane();
		presentation_pane = p.get_panel_prezentacji();
	}

	
	/**
	 * 
	 * Sekcja funkcji obslugujacych widok
	 */

	void addNoweListener(ActionListener e) {

		// System.out.print(Nowe.getName());

		Nowe.addActionListener(e);

		System.out.print(Nowe.getName());

	}
	
	void addDodajListener(ActionListener e)
	{
		dodajWierszButton.addActionListener(e);
		
	}
	
	void addUsunListener(ActionListener e)
	{
		usunWierszButton.addActionListener(e);
		
	}

	/*void addExecuteListener(ActionListener e) {
		wykonaj.addActionListener(e);
	}*/
	
	void addListaLIstener(ListSelectionListener e){
		listaTabel.addListSelectionListener(e);
	}
	
	void addTableModelChangedListener(TableModelListener e){
		table.getModel().addTableModelListener(e);
	}
	
	

	/*String pobierz_polecenie() {
		return obszar_polecenia.getText();
	}*/

	Model get_v_model() {
		return v_model;
	}

	JTable get_table() {
		return table;
	}

	JScrollPane JScrollPane() {
		return scrollPane;
	}

	void dodaj_scroll_z_tabela(JScrollPane a) {
		scrollPane=a;
		presentation_pane.setLayout(new GridLayout(1, 1));
		presentation_pane.add(scrollPane);

	}
	
	void requestFocusonListaTabel(){
		listaTabel.requestFocus();
	}

	void usun_scroll() {
		presentation_pane.remove(scrollPane);
	}
	
	void JList_validate(){
		listaTabel.validate();
	}

}
