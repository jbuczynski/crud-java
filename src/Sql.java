import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.swing.JOptionPane;


public class Sql {

	/**
	 * Tworzy połączenie do bazy danych, korzystając z właściwości zapisanych w
	 * pliku database.properties.
	 * 
	 * @return połączenie do bazy danych
	 */
	public static Connection getConnection() throws SQLException, IOException {
		Properties props = new Properties();
		FileInputStream in = new FileInputStream("database.properties");
		props.load(in);
		in.close();

		String drivers = props.getProperty("jdbc.drivers");
		if (drivers != null)
			System.setProperty("jdbc.drivers", drivers);
		String url = props.getProperty("jdbc.url");
		String username = props.getProperty("jdbc.username");
		String password = props.getProperty("jdbc.password");

		return DriverManager.getConnection(url, username, password);

	}
	
	
	public static void SaveToProperties(String url, String username,String password )
	{
		try {
		FileWriter file = new FileWriter("database.properties",
				false);// umozliwione nadpisanie
		BufferedWriter out = new BufferedWriter(file);

		
		String s = "jdbc.drivers=org.postgresql.Driver";

		
		out.write("jdbc.drivers=org.postgresql.Driver\r\n");
		out.write("jdbc.url=jdbc:postgresql://" + url + "\r\n");
		out.write("jdbc.username=" + username + "\r\n");
		out.write("jdbc.password=" + password + "\r\n");

		out.close();
		}
		catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
	public static Boolean Insert(String[][] tab,Connection conn)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(" INSERT INTO "+tab[0][0]+"( ");
		int i;
		for( i=0;i<tab[1].length;i++)
			sb.append(tab[1][i]+", ");
		sb.deleteCharAt(sb.length()-2);
		sb.append(") "+ "Values (");
		for( i=0;i<tab[2].length;i++)
			sb.append(tab[2][i]+", ");
		sb.deleteCharAt(sb.length()-2);
		
		sb.append(" )");
		String query = sb.toString();
		
		JOptionPane.showMessageDialog(null, query);
		
		try{
			  Statement stmt = conn.createStatement();
		     
		      stmt.executeUpdate(query);
			}
		
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
			return false;
		}
	return true;
	}
	
	
	
	public static Boolean Update(String table,String Value, String IDColumn, String Row,String currentcolumn,Connection conn)
	{
		String query = "UPDATE " + table + " SET " +currentcolumn +" = "+ Value + " WHERE "+IDColumn+" = "+ Row;
		JOptionPane.showMessageDialog(null, query);

		try{
			  Statement stmt = conn.createStatement();
		     
		      stmt.executeUpdate(query);
			}
			catch(SQLException e)
			{
				JOptionPane.showMessageDialog(null, e.getMessage());
				return false;
			}
		return true;
	}
	
	public static Boolean Delete(String query,Connection conn)
	{
		JOptionPane.showMessageDialog(null, query);
		
		try{
		  Statement stmt = conn.createStatement();
	     
	      stmt.executeUpdate(query);
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
			return false;
		}
		return true;
		
	}
	
	
}
