import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.sql.rowset.CachedRowSet;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import com.sun.rowset.CachedRowSetImpl;

public class Controller {

	private Model m_model;
	private View m_view;

	// ========================================================== constructor
	/** Constructor */
	Controller(Model model, View view) {
		m_model = model;
		m_view = view;

		// ... Add listeners to the view.
		m_view.addListaLIstener(new Lista_Listener());
		m_view.addNoweListener(new PolaczListener());
		//	m_view.addExecuteListener(new ExecuteListener());
		m_view.addDodajListener(new dodajListener());	
		m_view.addUsunListener(new usunListener());	
		
		m_view.addTableModelChangedListener(new TableModelChangedListener());
		m_view.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				try {
					if (m_model.get_Connection() != null)
						m_model.get_Connection().close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
		Sluchacz do przycisku, kt�y nawiazuje nowe polaczenie, zapisuje inforamacje o nim do modelu
		
		 */
	
	public void SaveProperties(String driver,String url,String username, String password)
	{
		try{
			FileWriter file = new FileWriter("database.properties",
					false);// umozliwione nadpisanie
			BufferedWriter out = new BufferedWriter(file);
	
		
			out.write( "jdbc.drivers="+driver+ "\r\n");
			out.write("jdbc.url=jdbc:postgresql://" +url+ "\r\n");
			out.write("jdbc.username=" + username + "\r\n");
			out.write("jdbc.password=" + password+ "\r\n");
			out.close();
		}
		catch(IOException e)
		{
			
		}
	
	}

	class PolaczListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			try {

				conn = Sql.getConnection();
				DatabaseMetaData meta = conn.getMetaData();
				if (meta.supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE)) {
					scrolling = true;
					m_model.SetScrolling(scrolling);

					stat = conn.createStatement(
							ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
				} else {
					stat = conn.createStatement();
					scrolling = false;
					m_model.SetScrolling(scrolling);
				}
				lm.clear();
				ResultSet tables = meta.getTables(null, null, null,
						new String[] { "TABLE" });
				// m_model.set_resultSet(tables);
				while (tables.next()) {
					lm.addElement(tables.getString(3));

				}
				tables.close();

			}

			catch (IOException a) {
				a.printStackTrace();
			} catch (SQLException a) {
				a.printStackTrace();
			}

			m_model.SetConnection(conn);
			m_model.SetStatement(stat);
			// m_model.scrolling();
			// m_model.get_lm();

		}

		private Connection conn;
		private Statement stat;
		private boolean scrolling;
		private DefaultListModel lm = m_model.get_lm();

	}
	
	class dodajListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			String table_name = (String) m_model.get_lm().getElementAt(m_model.GetSelectedListItem());
			tModel = m_model.get_TableModel();
			
			formularz = new JFrame();
			formularz.setTitle("Formularz dodawania");
			formularz.setLayout(new GridLayout(tModel.getColumnCount()+1, 1));
			wstaw = new JButton();
			wstaw.setText("Wstaw");
			wstaw.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					
					
					
					tModel.addRow( wczytaj_wartosci() );
					
				}
			});
			
			
			createForm();
			
			formularz.add(wstaw);
			
				
			
			formularz.pack();
			formularz.show();
			
		}
		
		private void createForm()
		{
			
			int  count = tModel.getColumnCount();
			tablicaWartosci = new Object[count];
			
			for(int i = 0; i<count;i++)
			{
			JPanel panel = new JPanel();
			panel.setName(Integer.toString(i));
			panel.setLayout( new GridLayout(1, 2) );
			panel.setBorder(BorderFactory.createEmptyBorder(0,10,10,10));;
			
			JLabel label = new JLabel();
			label.setName(Integer.toString(i));
			label.setText("Podaj warto�� dla pola: " + tModel.getColumnName(i) );
			
			JTextArea text = new JTextArea();
			text.setName(Integer.toString(i));	
			
			
			panel.add(label);
			panel.add(text);
			
			tablicaWartosci[i]= text;
			formularz.add(panel);
			}
			
		}
		
		private Vector wczytaj_wartosci()
		{
			
			Values = new Vector<String>();
			for(int i=0; i< tablicaWartosci.length;i++)
			Values.add( ((JTextArea)tablicaWartosci[i]).getText());
			
			return Values;
			
		}
		
		private JFrame formularz;
		private JButton wstaw;
		private DefaultTableModel tModel;
		private Object[] tablicaWartosci;
		private Vector<String> Values;
	}
	
	
	class usunListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
		
			DefaultTableModel model = m_model.get_TableModel();
			JTable table = m_view.get_table();
			String table_name = m_model.get_lm().getElementAt(m_model.GetSelectedListItem()).toString();
			String nazwaId = model.getColumnName(0);
			String ID = model.getValueAt(table.getSelectedRow(), 0).toString();
			
			//wywolac event kt�ry usuwania kt�ry odbiuerze sluchacz modeli
		
			
			
			//wyslac do klasy sql
			String query = "DELETE  FROM "+table_name+ " WHERE " + nazwaId + " = "+ ID;
			if(Sql.Delete(query,m_model.get_Connection()))
				model.removeRow(table.getSelectedRow());
			
		}
	}

	
	class TableModelChangedListener implements TableModelListener
	{
		
		@Override
		public void tableChanged(TableModelEvent e) {
			// TODO Auto-generated method stub
			 	
			//JOptionPane.showMessageDialog(m_view, e);
			 if (e.getType() == TableModelEvent.INSERT) {
				 int row = e.getFirstRow();  	 
				 DefaultTableModel model = (DefaultTableModel)e.getSource(); 
				 String[][] queryElements = new String[3][model.getColumnCount()];
				 
				 queryElements[0][0]=  m_model.get_lm().getElementAt(m_model.GetSelectedListItem()).toString();
				 
				 for(int i=0;i<model.getColumnCount();i++)
				 {
					 queryElements[1][i]=model.getColumnName(i);
				 }
				 
				 for(int i=0;i<model.getColumnCount();i++)
				 {
					 queryElements[2][i]= model.getValueAt(row, i).toString();
				 }
				 
				 if(! Sql.Insert(queryElements,m_model.get_Connection()))
					 model.removeRow(row);
             }
			 
			 
			 else if(e.getType() == TableModelEvent.UPDATE)
			 {
				 
				 	int row = e.getFirstRow();  
			        int column = e.getColumn();  
			       
			        DefaultTableModel model = (DefaultTableModel)e.getSource();  
			       
			        String id_column_name = model.getColumnName(0);
			        
			        String table_name = (String) m_model.get_lm().getElementAt(m_model.GetSelectedListItem());
			        JOptionPane.showMessageDialog(m_view, table_name);
			        		     		        
			        Sql.Update(table_name, (String)model.getValueAt(row, column),id_column_name, model.getValueAt(row,0).toString() , model.getColumnName(column),m_model.get_Connection());
				 
			 }
			 
			 else if(e.getType() == TableModelEvent.DELETE)
			 {
				 
				 
			 }
				
			
		}
		
				
		
		
	}
	//Sluchacz wyboru pola w liscie, wykonuje zapytanie 
	
	class Lista_Listener implements ListSelectionListener {

		
		 
		
		 
		@Override
		public void valueChanged(ListSelectionEvent e) {
		
			 scrollPane = m_view.JScrollPane();
			 rs = m_model.ResultSet();
			 table = m_view.get_table();
			 stat = m_model.Statement();
			
			
			
			JList lista = (JList) e.getSource();
			int i = lista.getSelectedIndex();
			m_model.SetSelectedListItem(i);
			if (m_model.get_lm() != null) {
				String nazwa = (String) m_model.get_lm().get(i);

			
				try {

					if (scrollPane != null)
						m_view.usun_scroll();

					if (rs != null)
						rs.close();
					// String query = m_view.pobierz_polecenie();
					rs = stat.executeQuery("Select * from " + nazwa);
					
					if (scrolling)
						model =  m_model.buildTableModel(rs);
						//model = new DefaultTableModel();
					
					else {
						CachedRowSet crs = new CachedRowSetImpl();
						crs.populate(rs);
						model = m_model.buildTableModel(crs);
					}

					table.setModel(model);
					
					scrollPane = new JScrollPane(table);
					m_view.dodaj_scroll_z_tabela(scrollPane);
					m_view.validate();

				} catch (SQLException u) {

					JOptionPane.showMessageDialog(m_view, u.getMessage());
				}
				m_view.addTableModelChangedListener(new TableModelChangedListener());
				m_model.SetResultSet(rs);
				m_model.Set_TableModel(model);
				//m_view.JList_validate();
			}
			
			// odswiezanie_ListyTabel();
		}
		
		
	
	
		private JScrollPane scrollPane ;
		private DefaultTableModel model;
		//private DefaultTableModel model;
		private ResultSet rs ;
		private boolean scrolling;
		private JTable table;
		private Statement stat;
		

	}

}
