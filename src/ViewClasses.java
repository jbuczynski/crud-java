import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ViewClasses {

}


class noweMenu {

	noweMenu(JFrame ramka) {

		Ramka = ramka;

		JMenuBar pasekm = new JMenuBar();
		Ramka.setJMenuBar(pasekm);
		JMenu menuPlik = new JMenu("Plik");
		pasekm.add(menuPlik);

		// do��cz do menu elementy Po��cz i Zamknij

		JMenuItem elemPolacz = new JMenuItem("Konfiguruj nowe po��czenie");
		elemPolacz.addActionListener(new Polaczenie());
		menuPlik.add(elemPolacz);
		
		JMenuItem dodajZdjecie = new JMenuItem("Dodaj zdj�cie");
		dodajZdjecie.addActionListener(new Zdjecie());
		menuPlik.add(dodajZdjecie);

		// element Zamknij ko�czy dzia�anie programu

		JMenuItem elemZamknij = new JMenuItem("Zamknij");
		elemZamknij.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarzenie) {
				System.exit(0);
			}
		});
		menuPlik.add(elemZamknij);

	}

	

	/**
	 * Element Po��cz wy�wietla okno dialogowe, pytaj�ce o has�o.
	 */

		private class Polaczenie implements ActionListener {
			public void actionPerformed(ActionEvent zdarzenie) {
				// je�eli to pierwsze wy�wietlenie, utw�rz okno
	
				if (okno == null)
					okno = new WyborHasla();
	
				// okre�l warto�ci domy�lne
				okno.zmienUzytkownika(new Uzytkownik("nazwa", null, null));
	
				// wy�wietl okno
				if (okno.wyswietlOkno(Ramka, "Po��cz")) {
					
	
						Uzytkownik u = okno.pobierzUzytkownika();
						String url = u.pobierzAdres();
						String username = u.pobierzImie();
						String password = new String(u.pobierzHaslo());
								
						Sql.SaveToProperties(url, username, password);
				}
			}
		}

		private class Zdjecie implements ActionListener {
			
			
			public void actionPerformed(ActionEvent zdarzenie) {
				// je�eli to pierwsze wy�wietlenie, utw�rz okno
			
				
				try {
	
					String ZdjPath = promptForFoto( (Component)zdarzenie.getSource());
					JOptionPane.showMessageDialog(Ramka, ZdjPath);
				}
	
				catch (Exception e) {
					JOptionPane.showMessageDialog(Ramka, e.getMessage());
				}
				
				
			}
			
			public String promptForFoto( Component parent )
			{	
				FileNameExtensionFilter imageFilter = new FileNameExtensionFilter(
					    "Image files", ImageIO.getReaderFileSuffixes());
				
			    JFileChooser fc = new JFileChooser();
			    fc.setFileFilter(imageFilter);
	
			    if( fc.showOpenDialog( parent ) == JFileChooser.APPROVE_OPTION )
			    {
			        return fc.getSelectedFile().getAbsolutePath();
			    }
	
			    return null;
			}
	
		}
	
	
	JFrame Ramka;
	private WyborHasla okno = null;
	
}





/**
 * Okno dialogowe wyboru has�a czyli JPanel tez moze yc jako okno jesli sie go
 * nie doda do content pane ramki, na to wychodzi bo wsywietla sie wczesniej niz
 * wywolywana jest funkcja wyswietl
 */

class WyborHasla extends JPanel {
	
	public WyborHasla() {
		setLayout(new BorderLayout());

		// tworzy panel, zawieraj�cy pola tekstowe "imi� u�ytkownika", oraz
		// "has�o"

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3, 3));
		panel.add(new JLabel("Nazwa u�ytkownika:"));
		panel.add(poleImie = new JTextField(""));
		panel.add(new JLabel("Has�o:"));
		panel.add(poleHaslo = new JPasswordField(""));
		panel.add(new JLabel("Adres bazy:"));
		panel.add(poleAdres = new JTextField(""));

		add(panel, BorderLayout.CENTER);

		// tworzy przyciski Ok i Anuluj, kt�re ko�cz� dzia�anie okna dialogowego

		JButton przyciskOk = new JButton("Ok");
		przyciskOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarzenie) {
				ok = true;
				okno.setVisible(false);
			}
		});

		JButton przyciskAnuluj = new JButton("Anuluj");
		przyciskAnuluj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarzenie) {
				okno.setVisible(false);
			}
		});

		// do��cza przyciski do po�udniowej kraw�dzi

		JPanel panelPrzyciskow = new JPanel();
		panelPrzyciskow.add(przyciskOk);
		panelPrzyciskow.add(przyciskAnuluj);
		add(panelPrzyciskow, BorderLayout.SOUTH);
	}

	/**
	 * Okre�la domy�lne warto�ci danych okna dialogowego
	 * 
	 * @param u
	 *            dane, dotycz�ce domy�lnego u�ytkownika
	 */
	public void zmienUzytkownika(Uzytkownik u) {
		poleImie.setText(u.pobierzImie());
	}

	/**
	 * Pobiera zawarto�� p�l okna dialogowego
	 * 
	 * @return obiekt u�ytkownika, kt�rego stan reprezentuje zawarto�� p�l okna
	 */
	public Uzytkownik pobierzUzytkownika() {
		return new Uzytkownik(poleImie.getText(), poleHaslo.getPassword(),
				poleAdres.getText());
	}

	/**
	 * Wy�wietla panel wyboru w oknie dialogowym
	 * 
	 * @param rodzic
	 *            komponent, kt�ry tworzy dane okno, lub null
	 * @param nazwa
	 *            nazwa okna dialogowego
	 */
	public boolean wyswietlOkno(Component rodzic, String nazwa) {
		ok = false;

		// znajd� ramk� w�a�ciciela

		Frame wlasciciel = null;
		if (rodzic instanceof Frame)
			wlasciciel = (Frame) rodzic;
		else
			wlasciciel = (Frame) SwingUtilities.getAncestorOfClass(Frame.class,
					rodzic);

		// je�li to pierwszy raz, lub zmieni� si� w�a�ciciel, zbuduj nowe okno

		if (okno == null || okno.getOwner() != wlasciciel) {
			wlasciciel = null;
			okno = new JDialog(wlasciciel, true);
			okno.getContentPane().add(this);
			okno.pack();
		}

		// okre�l nazw� okna i wy�wietl je

		okno.setTitle(nazwa);
		okno.show();
		return ok;
	}

	private JTextField poleImie;
	private JTextField poleAdres;
	private JPasswordField poleHaslo;
	private boolean ok;
	private JDialog okno;
}

/**
 * U�ytkownik posiada pola imie, oraz haslo. Ze wzgl�d�w bezpiecze�stwa haslo
 * jest przechowywane w zmiennej typu char[], a nie String.
 */
class Uzytkownik {
	public Uzytkownik(String aImie, char[] aHaslo, String aAdres) {
		imie = aImie;
		haslo = aHaslo;
		adres = aAdres;
	}

	public String pobierzImie() {
		return imie;
	}

	public char[] pobierzHaslo() {
		return haslo;
	}

	public String pobierzAdres() {
		return adres;
	}

	public void zmienImie(String aImie) {
		imie = aImie;
	}

	public void zmienHaslo(char[] aHaslo) {
		haslo = aHaslo;
	}

	public void zmienAdres(String aAdres) {
		adres = aAdres;
	}

	private String imie;
	private String adres;
	private char[] haslo;
}

class nowy_info_pane {

	nowy_info_pane(View ramka) {

		JPanel info_pane = new JPanel();
		info_pane.setLayout(new BorderLayout());
		// do��cz przycisk, pola tekstowe i etykiet�

		polaczButton = new JButton("Po��cz");
		polaczButton.setName("asf");
		System.out.print(polaczButton.getName());
		info_pane.add(polaczButton, BorderLayout.NORTH);

		JPanel panel_wewn = new JPanel();
		panel_wewn.setLayout(new BorderLayout());
		JLabel tableList = new JLabel("Lista tabel w bazie:      ");
		panel_wewn.add(tableList, BorderLayout.NORTH);
	
		listaTabel = new JList(ramka.get_v_model().get_lm());
		JScrollPane powPrzewijania = new JScrollPane(listaTabel);
		panel_wewn.add(powPrzewijania);

		info_pane.add(panel_wewn);
		
		info_pane.setBorder(BorderFactory.createEtchedBorder());
		ramka.add(info_pane, BorderLayout.WEST);
	
		
	}

	
	JButton wez_przycisk() {
		
		return polaczButton;
		
	}
	
	JList wez_liste(){
		return listaTabel;
	}
	
	//private DefaultListModel lm;
	private JButton polaczButton;
	private JList listaTabel;
}



 class add_execute_pane_and_presentation_pane
 {
	 add_execute_pane_and_presentation_pane(JFrame ramka){
		 JPanel panel = new JPanel();
		 panel.setLayout(new BorderLayout());
		 
		e = new nowy_execute_pane(panel);
		p = new nowy_presentation_pane(panel);
		
		
		ramka.add(panel);
			
		
	 }
	 
	 JButton dodajWierszButton(){
		 return e.dodajWierszButton();
	 }
	 JButton usunWierszButton(){
		 return e.usunWierszButton();
	 }
	 
	 JTable get_table(){
			return p.get_table();
	}
	 
	JScrollPane get_JScrollPane(){
		return p.get_JScrollPane();
	}
	
	JPanel get_panel_prezentacji(){
		return p.get_panel_prezentacji();
	}
		
	 
	 private nowy_execute_pane e;
	 private nowy_presentation_pane p;
 }


/**
 * Konstruktor do konkrenego wypadku mozna dodac inne ktore beda dodawac do roznorakich komponentow
 * @author Jakub
 *
 */
class nowy_execute_pane {

	
	
	nowy_execute_pane(JPanel ramka) {
		
		dodajWierszButton = new JButton("Dodaj wiersz");
		dodajWierszButton.setName("dodajWiersz");
		usunWierszButton = new JButton("Usun wiersz");
		usunWierszButton.setName("usunWiersz");
		
		panel_egzekucji = new JPanel();
		panel_egzekucji.add(dodajWierszButton);
		panel_egzekucji.add(usunWierszButton);
		
	ramka.add(panel_egzekucji, BorderLayout.SOUTH);
	}

   	JButton dodajWierszButton() {
		return dodajWierszButton;
	}

	JButton usunWierszButton() {
		return usunWierszButton;
	}
	
	private JPanel panel_egzekucji;
	private JButton dodajWierszButton;
	private JButton usunWierszButton;
}

class nowy_presentation_pane {
	nowy_presentation_pane(JPanel ramka) {

		panel_prezentacji = new JPanel();
		panel_prezentacji.setLayout(new GridLayout(1, 1));
		table = new JTable(new Object[][] { { "NO DATA" } },
				new Object[] { "SAMPLE CLUMN" });
		
		table.setCellSelectionEnabled(true);
		
	
	    
		scrollPane = new JScrollPane(table);
		scrollPane.createHorizontalScrollBar();
		panel_prezentacji.add(scrollPane);
		ramka.add(panel_prezentacji, BorderLayout.CENTER);
    
		
	}
	
	JTable get_table(){
		return table;
	}
	JScrollPane get_JScrollPane(){
		return scrollPane;
	}
	
	JPanel get_panel_prezentacji(){
		return panel_prezentacji;
	}
	
	
	private JPanel panel_prezentacji;
	private JScrollPane scrollPane;
	private JTable table;

}
