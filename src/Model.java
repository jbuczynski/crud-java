import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.table.DefaultTableModel;

//import com.sun.rowset.CachedRowSetImpl;

public class Model {

	Model() {

		lm = new DefaultListModel();
		
	}
	
	void set_resultSet(ResultSet result){
		rs =result;
	}

	DefaultListModel get_lm() {
		return lm;
		
	}
	
	void setLM(DefaultListModel l){
		 lm = l;
	}
	
	DefaultTableModel get_TableModel() {
		return TableModel;
	}
	
	void Set_TableModel(DefaultTableModel a){
				
		TableModel = a;
	}

	boolean scrolling() {
		return scrolling;
	}
	
	void SetScrolling(boolean a){
		scrolling = a;
	}
	

    Statement Statement() {
		return stat;
	}
	
	void SetStatement(Statement a){
		stat = a;
	}

	ResultSet ResultSet() {
		return rs;
	}
	
	void SetResultSet(ResultSet a){
		rs = a;
	}


	
	Connection get_Connection() {
		return conn;
	}
	
	void SetConnection(Connection a){
		conn = a;
	}
	
	void SetSelectedListItem(int li)
	{
		selectedListItem = li;
	}
	int GetSelectedListItem()
	{
		return selectedListItem;
	}
	
	public static DefaultTableModel buildTableModel(ResultSet rs)
	        throws SQLException {

	    ResultSetMetaData metaData = rs.getMetaData();

	    // names of columns
	    Vector<String> columnNames = new Vector<String>();
	    int columnCount = metaData.getColumnCount();
	    for (int column = 1; column <= columnCount; column++) {
	        columnNames.add(metaData.getColumnName(column));
	    }

	    // data of the table
	    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
	    while (rs.next()) {
	        Vector<Object> vector = new Vector<Object>();
	        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
	            vector.add(rs.getObject(columnIndex));
	        }
	        data.add(vector);
	    }

	    return new DefaultTableModel(data, columnNames);

	}
				
	private DefaultListModel lm;
	private DefaultTableModel TableModel;
	private ResultSet rs;
	private static Connection conn;
	private Statement stat;
	private boolean scrolling;
	private int selectedListItem;
	
}



